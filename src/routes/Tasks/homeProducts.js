const express = require('express');
const router = express.Router();
const modelProduct = require('../../models/products');

router.get('/', async(req, res) => {
    const products = await modelProduct.find({})
    res.send(products)
});

module.exports = router