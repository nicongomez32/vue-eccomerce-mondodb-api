const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs-extra');
const helpers = require('../../helpers/libs');
const modelProduct = require('../../models/products');

router.get('/addProduct', (req, res) => {
    res.send("received")
});
router.post('/addProduct', async(req, res) => {
    console.log(req.file);
    let obj = JSON.parse(JSON.stringify(req.body));
    console.log(obj);
    const extName = path.extname(req.file.originalname);
    const fileName = req.file.filename;
    const pathFile = req.file.path;
    const helpersName = helpers.aleatoryName(fileName);
    const targetPath = path.resolve(`C:/Users/Nico/Desktop/Tareas/Express-Vue/front/src/public/images/${helpersName}${extName}`);
    await fs.rename(pathFile, targetPath);
    const { name, description, price, stock } = req.body;
    const product = new modelProduct({
        precio: price,
        descripcion: description,
        nombre: name,
        descripcion: description,
        stock: stock,
        image: helpersName + extName
    })
    const productSave = await product.save()
    console.log(productSave);
    res.send("received")
});

module.exports = router