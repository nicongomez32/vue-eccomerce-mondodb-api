//Initialized
const express = require('express');
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const multer = require('multer');
const path = require('path');
const bodyParser = require('body-parser')

//Middlewares
app.use(express.json());
app.use(multer({ dest: (path.join(__dirname, '/temp')) }).single('image'));
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));

//Routes 
app.use(require('./routes/indexR.js'));
app.use('/products', require('./routes/Tasks/newProduct'));
app.use('/products', require('./routes/Tasks/homeProducts'));

//Sets
const config = require('./corsconfig');
app.use(cors(
    config.application.cors.server
));
require('./database/connection.js');
app.use(express.json());
app.set('port', process.env.PORT || 4000);

//Port
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
})