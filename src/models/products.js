const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    nombre: String,
    descripcion: String,
    precio: Number,
    stock: Number,
    image: {
        type: String,
        required: true,
    }
});

const Product = mongoose.model('Users', userSchema);

module.exports = Product