const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    nombre: String,
    descripcion: String,
});

const User = mongoose.model('Users', userSchema);

module.exports = User