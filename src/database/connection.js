const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/Express-Vue', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(db => console.log("Base conectada"))
    .catch(err => console.log(err));