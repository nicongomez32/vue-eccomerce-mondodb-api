const helpers = {};

helpers.aleatoryName = (name) => {
    var newName = "";
    for (let index = 0; index < 12; index++) {
        let alNum = Math.round(Math.random() * 10);
        newName += name.charAt(alNum);
    }
    return newName;
};
module.exports = helpers